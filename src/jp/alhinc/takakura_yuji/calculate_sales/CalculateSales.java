package jp.alhinc.takakura_yuji.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

class Branch{
	String code;
	String name;
	Long totals;
	public int length;

	public void setCode(String code) {
        this.code = code;
	}
	public void setName(String name) {
        this.name = name;
	}
	public void setSales(Long total) {
        this.totals = total;
	}
}

class CalculateSales {
	public static void main(String[] args) {
		HashMap<String,String> mapcode = new HashMap<>();
   		HashMap<String, Long> mapsales = new HashMap<>();

		BufferedReader br = null;
		try{
			File file = new File(args[0],"branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] branch = line.split(",");
				if(branch.length != 2 || !branch[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				mapcode.put(branch[0], branch[1]);
				mapsales.put(branch[0], 0l);
			}
			ArrayList<Integer> intList = new ArrayList<Integer>();
			File file1 = new File(args[0]);                                                  // 支店コードを読み込み
			File[] files = file1.listFiles();
			Arrays.sort(files);
			for (int i=0; i<files.length; i++) {
				String str = files[i].toString();                                       // 支店コードをString型に変更
				if(str.matches(".*[0-9]{8}.*rcd")){
						String ret =str.replaceAll("[^0-9]{8,}","");
						String result = ret.substring(1,8);
						int fn = Integer.parseInt(result);
						intList.add(fn);
						for(int z = 1; z < intList.size(); z++)
						if (intList.get(z) != intList.get(z-1) +1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
						FileReader fr1 = new FileReader(files[i]);
						br = new BufferedReader(fr1);
						String storecode = br.readLine();                                    // 支店コードを読み込み
						if(!mapsales.containsKey(storecode)) {                          // store_codeに一致しない場合
							System.out.println(result + "の支店コードが不正です");
							return;
						}
						String sales = br.readLine();                                             // 売上額を読み込み
						String line3 = br.readLine();                                          // 3桁以上の記載の有無
						if(line3 != null) {
							System.out.println(result + "のフォーマットが不正です");
							return;
						}
						long sales1 = Long.parseLong(sales);                           // 売上の金額をLong型に変更
						mapsales.put(storecode,mapsales.get(storecode)+sales1);          // 支店コードが同じものを足す
						long a = mapsales.get(storecode);
						if(a >= 1000000000) {                                                     // 10桁を超えた場合
							System.out.println("合計金額が10桁を超えました");
							return;
						}
				}
			}
		}catch(IOException e) {
			System.out.println("支店定義ファイルが存在しません");
			return;
        }finally {
        	if(br != null) {
        		try {
        			br.close();
        		}catch(IOException e) {
        			System.out.println("予期せぬエラーが発生しました");
        		}
        	}
        }

		Branch[] branches = new Branch[mapcode.size()];                                            // ファイルの出力
		int i1 = 0;
	    for (String code : mapsales.keySet()) {
	    	branches[i1] = new Branch();
	    	branches[i1].setCode(code);
	    	i1++;
	    }
	    int i2 = 0;
	    for (String name : mapcode.values()) {
	        branches[i2].setName(name);
	        i2++;
	    	}
	    int i3 = 0;
	    for (Long total : mapsales.values()) {
	    	branches[i3].setSales(total);
	    	i3++;
	    	}
	    try {
	    	File file2 = new File(args[0], "branch.out");
	    	FileWriter filewriter = new FileWriter(file2);
	    	BufferedWriter bw = new BufferedWriter(filewriter);
	    	for(int j = 0; j < branches.length; j++){
	    		bw.write(branches[j].code + "," + branches[j].name + ","+ branches[j].totals);                  // 表示
	    		bw.newLine();
	    		}
	    		bw.close();
	    } catch (IOException ex) {
	    	System.out.println("予期せぬエラーが発生しました");
	    }finally {
	       	if(br != null) {
	       		try {
	       			br.close();
	       		}catch(IOException e) {
	       			System.out.println("予期せぬエラーが発生しました");
	       		}
	       	}
	    }
	}
}